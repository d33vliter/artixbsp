**Script de BSPWM listo para su uso en ArtixLinux/Derivadas**


**Pesa 8MB solo por el Wallpaper, no sabia que era tan potente :D**

[![2021-01-09-1366x768-scrot.png](https://i.postimg.cc/7ZGVpzgy/2021-01-09-1366x768-scrot.png)](https://postimg.cc/sMz7Wv6n)

**TODO LO QUE CONTIENE ES DE LA VERSION 2.0**
[(Ir a releases)](https://gitlab.com/d33vliter/artixbsp/-/releases)
---

Recuerda volverlo ejecutable con `chmod +x artixbsp`

Ejecutalo con: `./artixbsp`


---

Algunos Paquetes que contiene:

Panel: **Tint2**

Gestor de ventanas: **BSPWM** 

Hokeys Daemon: **Sxhkd** [(Ver atajos de Sxhkd)](https://gitlab.com/d33vliter/artixbsp/-/blob/master/cfg/config/sxhkd/sxhkdrc)

Navegador: **Firefox**

Gestor de Archivos: **PCManFM**

Extractor de Archivos: **File-Roller**

Network Manager: **Connman-gtk**

Terminal: **Sakura**

Editor de Textos: **Leafpad**

Reproductor Multimedia: **MPV**

Administrador de dispositivos de audio: **Pulsemixer**

Applet de volumen: **Volumeicon**

Gestor de Wallpaper: **Nitrogen**

Cambiar Resolución de Pantalla: **Arandr**

Para cambiar apariencia: **Lxappearance**
